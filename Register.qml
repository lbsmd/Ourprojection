import QtQuick 2.0
import QtQuick.Controls 2.2
import "."
import FileIO 1.0

Item {
    property double labelsize: 0.3
    property double unitRowHeight: height * 0.05
    Image {
        id: background
        width: parent.width
        height: parent.height
        source: "background.jpg"
        Rectangle {
            id: userNameRow
            x: parent.width * 0.1
            y: height + 20
            width: parent.width * 0.8
            height: unitRowHeight
            color: "#b38a8a8a"
            Text {
                id: unmlabel
                width: parent.width * labelsize
                height: parent.height
                text: qsTr("用户名")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight
                font.pixelSize: 22
            }
            TextInput {
                id: username
                anchors.right: parent.right
                width: parent.width * (1 - labelsize)
                height: parent.height
                clip: true
                font.pixelSize: 22
            }
        }
        Rectangle {
            id: passwdRow
            x: parent.width * 0.1
            y: (height + 20) * 2
            width: parent.width * 0.8
            height: unitRowHeight
            color: "#b38a8a8a"
            Text {
                id: pwdlabel
                width: parent.width * labelsize
                height: parent.height
                text: qsTr("密  码")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight
                font.pixelSize: 22
            }
            TextInput {
                id: passwd
                anchors.right: parent.right
                width: parent.width * (1 - labelsize)
                height: parent.height
                echoMode: TextInput.Password
                clip: true
                font.pixelSize: 22
            }
        }
        Rectangle {
            id: repasswdRow
            x: parent.width * 0.1
            y: (height + 20) * 3
            width: parent.width * 0.8
            height: unitRowHeight
            color: "#b38a8a8a"
            Text {
                id: repwdlabel
                width: parent.width * labelsize
                height: parent.height
                text: qsTr("确认密码")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight
                font.pixelSize: 22
            }
            TextInput {
                id: repasswd
                anchors.right: parent.right
                width: parent.width * (1 - labelsize)
                height: parent.height
                echoMode: TextInput.Password
                clip: true
                font.pixelSize: 22
            }
        }
        Rectangle {
            id: sexRow
            x: parent.width * 0.1
            y: (height + 20) * 4
            width: parent.width * 0.8
            height: unitRowHeight
            color: "#b38a8a8a"
            Text {
                id: sexlabel
                width: parent.width * labelsize
                height: parent.height
                text: qsTr("性  别")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight
                font.pixelSize: 22
            }

            RadioButton {
                id: man
                x: sexlabel.x + sexlabel.width
                y: sexlabel.y
                width: parent.width * 0.2
                height: parent.height
                text: qsTr("男")
                checked: true
                font.pointSize: 22
                scale: 0.7
            }

            RadioButton {
                id: wuman
                x: sexlabel.x + sexlabel.width + man.width
                y: sexlabel.y
                width: parent.width * 0.2
                height: parent.height
                text: qsTr("女")
                font.pointSize: 22
                scale: 0.7
            }
        }
        Rectangle {
            id: ageRow
            x: parent.width * 0.1
            y: (height + 20) * 5
            width: parent.width * 0.8
            height: unitRowHeight
            color: "#b38a8a8a"
            Text {
                id: agelabel
                width: parent.width * labelsize
                height: parent.height
                text: qsTr("年  龄")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight
                font.pixelSize: 22
            }
            TextInput {
                id: age
                anchors.right: parent.right
                width: parent.width * (1 - labelsize)
                height: parent.height
                clip: true
                font.pixelSize: 22
            }
        }
        Rectangle {
            id: mailRow
            x: parent.width * 0.1
            y: (height + 20) * 6
            width: parent.width * 0.8
            height: unitRowHeight
            color: "#b38a8a8a"
            Text {
                id: maillabel
                width: parent.width * labelsize
                height: parent.height
                text: qsTr("邮  箱")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight
                font.pixelSize: 22
            }
            TextInput {
                id: mail
                anchors.right: parent.right
                width: parent.width * (1 - labelsize)
                height: parent.height
                clip: true
                font.pixelSize: 22
            }
        }
        Rectangle {
            id: phoneRow
            x: parent.width * 0.1
            y: (height + 20) * 7
            width: parent.width * 0.8
            height: unitRowHeight
            color: "#b38a8a8a"
            Text {
                id: phonelabel
                width: parent.width * labelsize
                height: parent.height
                text: qsTr("电  话")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight
                font.pixelSize: 22
            }
            TextInput {
                id: phone
                anchors.right: parent.right
                width: parent.width * (1 - labelsize)
                height: parent.height
                clip: true
                font.pixelSize: 22
            }
        }
        ToastRec {
            id: regToast
            x: parent.width / 2 - regToast.width / 2
            y: (unitRowHeight + 20) * 9
            width: parent.width * 0.3
            height: unitRowHeight
        }
        Connections{
            target: GroupC
            onUserLoginSuccessed:{
                console.log(("登录成功"))
                regFile.write(username.text+"\n"+passwd.text)
                var curUser=GroupC.getUserInfo()
                GlobalVar.currentUserInfo=curUser
                GlobalVar.currentUserId=curUser.objectId
                mainWin.source="linkmain.qml"
            }
        }
        Connections{
            target: GroupC
            onUserLoginFailed:{
                console.log("登录失败")
            }
        }
        Connections {
            target: GroupC
            onCreateUserSuccessed: {
                console.log("注册成功")
                regFile.write(phone.text+'\n'+passwd.text)
                GroupC.userLogin(phone.text,passwd.text)
            }
        }
        Connections {
            target: GroupC
            onCreateUserFailed: {
                console.log("注册失败:"+msg)
                regToast.showToast("注册失败", 2000)
            }
        }

        Rectangle {
            id: regbtn
            x: parent.width * 0.1
            y: (height + 20) * 8
            width: parent.width * 0.8
            height: unitRowHeight
            color: "#b387cefa"
            radius: 8
            Text {
                id: btnT
                anchors.centerIn: parent
                width: parent.width
                height: parent.height
                text: qsTr("注册")
                font.pointSize: 22
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            FileIO {
                id: regFile
                source: "uphone"
            }

            MouseArea {
                width: parent.width
                height: parent.height
                onClicked: {
                    var uname = username.text
                    if (uname.length < 1) {
                        console.log("用户名为空")
                        regToast.showToast("用户名为空", 2000)
                        return
                    }
                    var upswd = passwd.text
                    var urpwd = repasswd.text
                    if (upswd != urpwd) {
                        console.log("两次密码不同")
                        regToast.showToast("两次密码不同", 2000)
                        return
                    }
                    var usex = man.checked ? "男" : "女"
                    var uage = age.text
                    if (uage.length < 1) {
                        console.log("年龄为空")
                        regToast.showToast("年龄为空", 2000)
                        return
                    }
                    var umail = mail.text
                    if (umail.length < 1) {
                        console.log("邮箱为空")
                        regToast.showToast("邮箱为空", 2000)
                        return
                    }

                    var uphone = phone.text
                    if (uphone.length < 1) {
                        console.log("电话为空")
                        regToast.showToast("电话为空", 2000)
                        return
                    }
                    GroupC.createUser(uname, upswd, usex, uage,
                                      umail, uphone)
                }
            }
        }
    }
}
