import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.2

Page {
    id: window
    visible: true
    width:Screen.width
    height:Screen.height
    title: "已加入的签到组"

    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height

        Column {
            anchors.fill: parent

//            ItemDelegate {
//                text: qsTr("GROUP")
//                width: parent.width
//                onClicked: {
//                    stackView.push("page1.qml")
//                    drawer.close()
//                }
//            }
            ItemDelegate {
                text: qsTr("个人信息")
                width: parent.width
                onClicked: {
                    stackView.push("page2.qml")
                    drawer.close()
                }
            }
            ItemDelegate {//申请加组页面
                text: qsTr("申请加组页面")
                width: parent.width
                onClicked: {
                    stackView.push("addGroupPage.qml")
                    drawer.close()
                }
            }
            ItemDelegate {//修改信息页面
                text: qsTr("修改信息页面")
                width: parent.width
                onClicked: {
                    stackView.push("changemyinfor.qml")
                    drawer.close()
                }
            }
            ItemDelegate {//创建一个签到组
                text: qsTr("创建一个签到组")
                width: parent.width
                onClicked: {
                    stackView.push("createGroup.qml")
                    drawer.close()
                }
            }
//            ItemDelegate {//修改组信息
//                text: qsTr("修改组信息")
//                width: parent.width
//                onClicked: {
//                    stackView.push("changeGroupInfor.qml")
//                    drawer.close()
//                }
//            }
//            ItemDelegate {//查看组信息
//                text: qsTr("组信息")
//                width: parent.width
//                onClicked: {
//                    stackView.push("groupInformation.qml")
//                    drawer.close()
//                }
//            }
            ItemDelegate {//quit
                text: qsTr("注销")
                width: parent.width
                onClicked: {
                    drawer.close()
                }
            }
            ItemDelegate {//quit
                text: qsTr("退出")
                width: parent.width
                onClicked: {
                    stackView.push("quitButton.qml")
                    drawer.close()
                    Qt.quit()
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "page1.qml"
        anchors.fill: parent
    }
}
