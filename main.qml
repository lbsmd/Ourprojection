import QtQuick 2.8
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import "."
import FileIO 1.0

Window {
    id: rootWin
    visible: true
    width: Screen.width
    height: Screen.height
    //    width: 400
    //    height: 500
    //    minimumHeight: 500
    //    minimumWidth: 400
    title: qsTr("欢迎")
    property string userid: "lajdlfasld"
    Loader {
        id: mainWin
        anchors.fill: parent
        width: parent.width
        height: parent.height
        source: "Welcome.qml"
        focus: true
        Keys.onEscapePressed: {
            console.log("当前界面:" + mainWin.source)
            Qt.quit()
        }
        Keys.onPressed: {
            if (event.key == Qt.Key_Back) {
                console.log("当前界面:" + mainWin.source)
                Qt.quit()
            }
        }
    }
    FileIO{
        id: uphoneFile
        source:"uphone"
    }
    Timer {
        interval: 20
        running: true
        repeat: false
        onTriggered: {
            if(uphoneFile.isExited()){
                console.log("本地账户信息:"+uphoneFile.read())
                var userObj=JSON.parse(uphoneFile.read())
                console.log("电话:"+userObj[0]+"密码:"+userObj[1])
                var curUser=GroupC.getUserInfo()
                console.log("用户JSON串:"+JSON.stringify(curUser))
                GlobalVar.currentUserInfo=curUser
                GlobalVar.currentUserId=curUser.objectId
                mainWin.source="linkmain.qml"
            }else{
                mainWin.source = "Login.qml"
                rootWin.title = "登录"
            }
        }
    }
}
