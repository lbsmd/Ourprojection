#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QIcon>
#include "fileio.h"
#include <QDebug>

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);
    qmlRegisterType<FileIO>("FileIO", 1, 0, "FileIO");
    QQmlApplicationEngine engine;
    qDebug()<<engine.offlineStoragePath();
    app.setWindowIcon(QIcon(":/3.jpg"));
    app.setApplicationName("test");
    app.setOrganizationDomain("wanywhn.com.cn");
    app.setOrganizationName("wuxun");



    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    //    engine.load(QUrl(QStringLiteral("qrc:/getgoal.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
