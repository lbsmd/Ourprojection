import QtQuick 2.0

Item {
    width: 100
    height: 100
    Image {
        id: image
        anchors.fill: parent
        width: parent.width
        height: parent.height
        source: "back.jpg"

        Text {
            id: text1
            y: parent.height/6
            width: parent.width
            height: parent.height*0.1
            text: qsTr("欢迎界面")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: text1.height*0.8
        }
    }

}
