import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import "."

Page {
    title: qsTr("月排行榜")
    id: rankPage
    width: Screen.width
    height: Screen.height
    ListModel {
        id: rankModel
    }
    Component {
        id: rankDelegate
        Rectangle {
            id: itemRoot
            width: rankList.width
            height: rankList.height
            color: "#00000000"
            Rectangle {
                id: titleContainer
                width: parent.width
                height: parent.height * 0.1
                color: "#00000000"
                Text {
                    anchors.fill: parent
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: year + "年" + month + "月"
                    font.pixelSize: 22
                    color: "#ffffff"
                }
            }
            Rectangle {
                id: gridContainer
                y: titleContainer.y + titleContainer.height
                width: parent.width
                height: parent.height * 0.9
                color: "#00000000"
                Grid {
                    id: monthGrid
                    anchors.fill: parent
                    width: parent.width
                    height: parent.height
                    rows: 6
                    columns: 7
                    spacing: (width * 2 / 9) / 12
                    flow: Grid.LeftToRight
                    add: Transition {
                        //加载时候的过度方式，不设置这属性的话，一进入该界面可看到布局
                        NumberAnimation {
                            properties: "x"
                            duration: 1000
                        }
                    }
                    move: Transition {
                        //子项由可见与不可见之间切换执行的过度
                        NumberAnimation {
                            properties: "x"
                            duration: 1000
                        }
                    }
                    Repeater {
                        //重复器，这里就不做具体介绍，当然我们也可以使用多个子项布局，效果一样
                        model: 42
                        Rectangle {
                            id: itemBackGround
                            color: (index < 9 ? "0" + (index + 1) : "" + (index + 1))
                            in day ? "#00CD00" : "#00000000"
                            radius: itemBackGround.height / 2
                            width: parent.width / 8
                            height: parent.height / 9
                            visible: index > 30 ? false : true
                            Text {
                                anchors.fill: parent
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                font.pixelSize: 12
                                text: qsTr("" + (index + 1))
                                color: "#ffffff"
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    console.log("星期数据:" + JSON.parse(
                                                    rankModel.weekTag)["1"])
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    Rectangle {
        id: listRoot
        anchors.fill: parent
        Image {
            id: listBg
            anchors.fill: parent
            source: "sign_bg.png"
            ListView {
                id: rankList
                width: parent.width
                height: parent.height
                model: rankModel
                delegate: rankDelegate
            }
        }
    }
    Connections {
        target: GroupC
        onGetSignedHistoryOfSuccessed: {
            console.log("收到历史签到串:" + list)
            var listObj = JSON.parse(list)
            for (var i = 0; i < listObj.length; i++) {
                var signTag = {
                    day: {

                    }
                }
                var signTime = String(listObj[i].signintime.iso).toString()
                console.log("日期串:" + signTime)
                var year = signTime.substring(0, 4)
                console.log("解析年:" + year)
                if (!("year" in signTag)) {
                    signTag.year = year
                } else {
                    if (signTag.year !== year) {
                        rankModel.append(signTag)
                        signTag = {
                            day: {

                            }
                        }
                    }
                }
                var month = signTime.substring(5, 7)
                console.log("解析月:" + month)
                if (!("month" in signTag)) {
                    signTag.month = month
                } else {
                    if (signTag.month !== month) {
                        rankModel.append(signTag)
                        signTag = {
                            day: {

                            }
                        }
                    }
                }
                var day = signTime.substring(8, 10)
                console.log("解析日:" + day)
                signTag.day[day] = 1
            }
            rankModel.append(signTag)
        }
    }
    Connections {
        target: GroupC
        onGetSignedHistoryOfFailed: {
            console.log("获取历史签到失败:" + list)
        }
    }

    Component.onCompleted: {
        console.log("当前用户id:" + GlobalVar.currentUserId)
        console.log("当前组id:" + GlobalVar.currentGroupId)
        GroupC.getSignedHistoryOf(GlobalVar.currentUserId,
                                  GlobalVar.currentGroupId,
                                  new Date(2000, 1, 1))
    }
}
