import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import "."

Page {
    id: changeGroupInforPage
    title: qsTr("修改组信息")
    width: Screen.width
    height: Screen.height
    Rectangle {
        height: parent.height
        width: parent.width
        Column {
            width: parent.width
            height: parent.height * 14 / 16
            //填写组名
            Rectangle {
                width: parent.width
                height: parent.height / 8
                TextField {
                    id: changeGroupName
                    anchors.right: parent.right
                    height: parent.height
                    width: parent.width * 4 / 5
                    placeholderText: "基佬组"
                    color: "red"
                    focus: true
                    font.pixelSize: 34
                }
                Rectangle {
                    id: changegroupnamewordrect
                    height: parent.height
                    width: parent.width / 5
                    color: "white"
                    Text {
                        id: changeGroupNametext
                        text: qsTr("组名")
                        anchors.centerIn: parent
                        font.pixelSize: 34
                    }
                }
            }
//            //填写签到地点
//            Rectangle {
//                width: parent.width
//                height: parent.height / 8
//                TextField {
//                    id: createGroupAdd
//                    anchors.right: parent.right
//                    height: parent.height
//                    width: parent.width * 4 / 5
//                    placeholderText: "逸夫楼"
//                    color: "red"
//                    focus: true
//                    font.pixelSize: 34
//                }
//                Rectangle {
//                    id: creategroupAddwordrect
//                    height: parent.height
//                    width: parent.width / 5
//                    color: "white"
//                    Text {
//                        id: changeGroupAddtext
//                        text: qsTr("签到地址")
//                        anchors.centerIn: parent
//                        font.pixelSize: 34
//                    }
//                }
//            }
            //签到目的
            Rectangle {
                width: parent.width
                height: parent.height / 8
                TextField {
                    id: createGroupintence
                    anchors.right: parent.right
                    height: parent.height
                    width: parent.width * 4 / 5
                    placeholderText: "为了搞基签到"
                    color: "red"
                    focus: true
                    font.pixelSize: 34
                }
                Rectangle {
                    id: creategroupintencewordrect
                    height: parent.height
                    width: parent.width / 5
                    color: "white"
                    Text {
                        id: changeGroupintencetext
                        text: qsTr("签到目的")
                        anchors.centerIn: parent
                        font.pixelSize: 34
                    }
                }
            }
            //签到截止日期
            Rectangle {
                width: parent.width
                height: parent.height / 8
                Rectangle {
                    id: creategroupDatewordrect
                    height: parent.height
                    width: parent.width / 5
                    color: "white"
                    Text {
                        id: changeGroupDatetext
                        text: qsTr("签到目的")
                        anchors.centerIn: parent
                        font.pixelSize: 34
                    }
                }
                Rectangle {
                    height: parent.height
                    width: parent.width * 4 / 5
                    anchors.right: parent.right
                    Row {
                        height: parent.height
                        width: parent.width
                        SpinBox {
                            //year
                            id: year
                            width: parent.width / 3
                            height: parent.height / 2
                            editable: true
                            from: 2018
                            to: 2050
                            anchors.verticalCenter: parent.verticalCenter
                        }
                        SpinBox {
                            //month
                            id: month
                            width: parent.width / 3
                            height: parent.height / 2
                            editable: true
                            from: 1
                            to: 12
                            anchors.verticalCenter: parent.verticalCenter
                        }
                        SpinBox {
                            //day
                            id: day
                            width: parent.width / 3
                            height: parent.height / 2
                            editable: true
                            from: 1
                            to: 31
                            anchors.verticalCenter: parent.verticalCenter
                        }
                    }
                }
            }
            Connections{
                target: GroupC
                onUpdateGroupInfoSuccessed:{
                    console.log("更新组信息成功")
                }
            }
            Connections{
                target: GroupC
                onUpdateGroupInfoFailed:{
                    console.log("更新组信息失败")
                }
            }
            //提交按钮
            Rectangle {
                id: createGroupButton
                width: parent.width / 5
                height: parent.width / 10
                radius: 50
                color: "red"
                anchors.horizontalCenter: parent.horizontalCenter
                Text {
                    id: createGroupButtonText
                    text: qsTr("确认修改")
                    font.pixelSize: 34
                    anchors.centerIn: parent
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked:{
                        createGroupButton.color = "green"
                        var gcname=changeGroupName.text
                        var gcgoal=createGroupintence.text
                        var gcyear=year.value
                        var gcmonth=month.value
                        var gcday=day.value
                        var timeStr=""+gcyear+"-"+(gcmonth<10?"0":"")+gcmonth+"-"+(gcday<10?"0":"")+gcday
                        console.log("组列表JSON串:"+GlobalVar.userGroupsObject)
                        var cGroupList=JSON.parse(GlobalVar.userGroupsObject)
                        console.log("当前组Index:"+GlobalVar.currentGroupIndex)
                        var cGroupObj=JSON.parse(JSON.stringify(cGroupList[Number(GlobalVar.currentGroupIndex)]))
                        cGroupObj.groupName=gcname
                        cGroupObj.goal=gcgoal

                        var gtemp=String(cGroupObj.deadline.iso).toString()
                        var temp=""
                        console.log("目标日期:"+timeStr)
                        for(var i=0;i<gtemp.length;i++){
                            if(i>=timeStr.length){
                                temp+=gtemp[i]
                            }else{
                                temp+=timeStr[i]
                            }
                        }

                        console.log("更新后日期:"+temp)
                        cGroupObj.deadline.iso=temp

                        console.log("更新组信息:"+JSON.stringify(cGroupObj))
                        GroupC.updateGroupInfo(cGroupObj)
                    }
                }
            }
        }
    }
}
