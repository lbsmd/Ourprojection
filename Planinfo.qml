import QtQuick 2.0

Item {
    property double labelsize: 0.3
    Image {
        id: image
        anchors.fill: parent
        width: parent.width
        height: parent.height
        source: "background.jpg"
    }
}
