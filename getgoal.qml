import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import FileIO 1.0
import "."

//签到界面
import QtQuick.Layouts 1.3

Page {
    width: Screen.width
    height: Screen.height
    contentWidth: -1
    visible: false
    id: goalpage
    title: qsTr("getgoalpage")
    Rectangle {
        id: goalImgRoot
        z: 100
        visible: false
        width: Screen.width
        height: Screen.height
        color: "#80000000"
        anchors.fill: parent
        Image {
            id: goalSuccess
            source: "suc_title.png"
            width: parent.width * 0.6
            height: parent.height * 0.2
            transformOrigin: Item.Center
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            scale: 1
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                goalImgRoot.visible = false
            }
        }
    }
    Rectangle {
        id: colRoot
        anchors.fill: parent
        Image {
            id: colBg
            anchors.fill: parent
            width: parent.width
            height: parent.height
            source: "sign_bg.png"
            Column {
                id: contentRoot
                anchors.fill: parent
                Rectangle {
                    //每日签到图像显示
                    width: parent.width
                    color: "#00000000"
                    height: parent.height * 0.4
                    Image {
                        //Image
                        source: "qrc:/2.jpg"
                        anchors.fill: parent
                    }
                }
                Rectangle {
                    //还有每日励志语句
                    width: goalpage.width
                    height: goalpage.height / 8
                    color: "orange"
                    anchors.horizontalCenter: parent.horizontalCenter
                    Text {
                        id: comewords
                        text: qsTr("有志者事竟成")
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pixelSize: 30
                    }
                }
                //签到按钮
                FileIO {
                    id: goalFile
                    property string date
                    source: "goalStatus"
                }
                Connections {
                    target: GroupC
                    onGoalGetSuccessed: {
                        console.log("签到成功:" + msg)
                        goalFile.write(goalFile.date)
                        goalButtontext.text = qsTr("已签到")
                        goalButton.color = "green"
                        goalImgRoot.visible = true
                    }
                }
                Connections {
                    target: GroupC
                    onGoalGetFailed: {
                        console.log("签到失败:" + msg)
                    }
                }
                Rectangle {
                    width: goalpage.width
                    height: goalpage.height / 10
                    color: "#00000000"
                    //getgoalButton
                    Rectangle {
                        id: goalButton
                        width: goalpage.height / 8
                        height: goalpage.height / 8
                        radius: goalpage.height / 8
                        color: "red"
                        anchors.horizontalCenter: parent.horizontalCenter

                        Text {
                            id: goalButtontext
                            text: "签到"
                            font.pixelSize: 22
                            anchors.centerIn: parent
                            property string groupid
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                if (goalFile.isExited()) {
                                    var goalObj = JSON.parse(goalFile.read())
                                    var currDate = new Date()
                                    console.log("签到时间:" + goalFile.read())
                                    console.log("本地时间:" + currDate.getDate())
                                    for (var i = 3; i < goalObj.length; i += 4) {
                                        if (goalObj[i] === GlobalVar.currentGroupId) {
                                            if (Number(goalObj[i - 3]) > currDate.getFullYear(
                                                        )) {
                                                console.log("签到年份不合逻辑")
                                                goalButtontext.text = qsTr(
                                                            "年份错误")
                                                return
                                            }
                                            if (Number(goalObj[i - 2]) > (currDate.getMonth(
                                                                              ) + 1)) {
                                                console.log("签到月份不和逻辑")
                                                goalButtontext.text = qsTr(
                                                            "月份错误")
                                                return
                                            }
                                            if (Number(goalObj[i - 1]) >= (currDate.getDate())) {
                                                console.log("今日已签到")
                                                goalButtontext.text = qsTr(
                                                            "已签到")
                                                goalButton.color = "green"
                                                return
                                            }
                                        }
                                    }
                                }

                                var date = new Date()
                                var year = date.getFullYear()
                                var month = date.getMonth() + 1
                                var day = date.getDate()
                                console.log("小时:" + date.getHours(
                                                ) + "分钟:" + date.getMinutes(
                                                ) + "秒:" + date.getSeconds())
                                var goalTime = year + '\n' + month + '\n' + day
                                        + '\n' + GlobalVar.currentGroupId + '\n'
                                console.log("签到日期:" + goalTime)
                                goalFile.date = goalTime
                                date.setFullYear(date.getFullYear(),
                                                 date.getMonth + 1,
                                                 date.getDate())
                                GroupC.goalGet(GlobalVar.currentUserId,
                                               GlobalVar.currentGroupId, date)
                            }
                        }
                    }
                }
                Component.onCompleted: {
                    if (goalFile.isExited()) {
                        var goalObj = JSON.parse(goalFile.read())
                        var currDate = new Date()
                        for (var i = 3; i < goalObj.length; i += 4) {
                            if (goalObj[i] === GlobalVar.currentGroupId) {
                                if (Number(goalObj[i - 3]) > currDate.getFullYear(
                                            )) {
                                    console.log("签到年份不合逻辑")
                                    goalButtontext.text = qsTr("年份错误")
                                }
                                if (Number(goalObj[i - 2]) > (currDate.getMonth(
                                                                  ) + 1)) {
                                    console.log("签到月份不和逻辑")
                                    goalButtontext.text = qsTr("月份错误")
                                }
                                if (Number(goalObj[i - 1]) >= (currDate.getDate(
                                                                   ))) {
                                    console.log("今日已签到")
                                    goalButtontext.text = qsTr("已签到")
                                    goalButton.color = "green"
                                }
                            }
                        }
                    }
                }
                Rectangle {
                    height: goalpage.height * 0.03
                    width: goalpage.width
                    color: "#00000000"
                }
                Rectangle {
                    //选择查看的信息
                    height: goalpage.height * 0.1
                    width: goalpage.width / 2
                    anchors.horizontalCenter: parent.horizontalCenter
                    ComboBox {
                        id: goalComboBox
                        anchors.fill: parent
                        height: goalpage.width * 8 / 120
                        model: ["查看我的签到情况", "查看全员今日签到", "查看全员签到统计", "查看组信息"]
                        font.pixelSize: 30
                        onCurrentIndexChanged: {
                            if (goalComboBox.currentIndex == 0) {
                                console.log("Choose My goal")
                                //查看我的签到信息统计
                            }
                            if (goalComboBox.currentIndex == 1) {
                                console.log("Choose All goal")
                                //跳转到全员今日签到统计
                            }
                            if (goalComboBox.currentIndex == 2) {
                                console.log("查看所有人签到统计")
                                //跳转到全员签到统计。可能近一个月
                            }
                        }
                    }
                }
                Rectangle {
                    height: goalpage.height * 0.02
                    width: goalpage.width
                    color: "#00000000"
                }

                Rectangle {
                    //查看签到情况按钮
                    id: checkButton
                    width: parent.width / 5
                    height: parent.width / 15
                    radius: parent.width / 30
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: "orange"
                    Text {
                        id: seegoals
                        text: "查看"
                        anchors.centerIn: parent
                        font.pixelSize: 30
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log(rootWin.userid)
                            if (goalComboBox.currentIndex == 0) {
                                console.log("See My goals")
                                stackView.push("MonthRank.qml")
                            }
                            if (goalComboBox.currentIndex == 1) {
                                console.log("See All goals")
                                stackView.push("DayRank.qml")
                            }
                            if (goalComboBox.currentIndex == 3) {
                                console.log("跳转组信息页面")
                                stackView.push("groupInformation.qml")
                                //跳转到组信息页面
                            }
                        }
                    }
                }
            }
        }
    }
}
