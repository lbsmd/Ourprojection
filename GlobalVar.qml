pragma Singleton
import QtQuick 2.0

Item {
    property string currentGroupId
    property int currentGroupIndex
    property QtObject currentGroupInfo
    property string userGroupsObject
    property QtObject currentUserInfo
    property string currentUserId
}
