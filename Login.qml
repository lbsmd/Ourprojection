import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import "."
import FileIO 1.0

Item {
    property double unitRowHeight:Screen.height*0.05
    FileIO{
        id:logFile
        source: "uphone"
    }
    Connections{
        target: GroupC
        onUserLoginSuccessed:{
            console.log(("登录成功"))
            logFile.write(username.text+"\n"+password.text)
            var curUser=GroupC.getUserInfo()
            GlobalVar.currentGroupInfo=curUser
            GlobalVar.currentUserId=curUser.objectId
            mainWin.source="linkmain.qml"
        }
    }
    Connections{
        target: GroupC
        onUserLoginFailed:{
            console.log("登录失败")
            logToast.showToast("账号或密码错误",2000)
        }
    }

    Image {
        id: bckground
        width: parent.width
        height: parent.height
        source: "background.jpg"

        Rectangle {
            id: logbtn
            color: "#6663b8ff"
            radius: 8
            x: parent.width*0.1
            y: parent.height/2+logbtn.height
            width: parent.width*0.8
            height: unitRowHeight
            Text {
                id: btnT
                anchors.fill: parent
                width: parent.width
                height: parent.height
                text: qsTr("登   录")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: btnT.height*0.8
            }
            MouseArea{
                width: parent.width
                height: parent.height
                onClicked: {
                    var uname=username.text
                    if(uname.length<1){
                        logToast.showToast("用户名为空",2000)
                        return
                    }
                    var upswd=password.text
                    if(uname.length<1){
                        logToast.showToast("密码为空",2000)
                        return
                    }
                    GroupC.userLogin(uname,upswd)
                }
            }
        }
        ToastRec{
            id: logToast
            x: parent.width/2-logToast.width/2
            y: unitRowHeight*3
            width: parent.width*0.3
            height: unitRowHeight
        }

        Rectangle {
            id: usernmrow
            color: "#00000000"
            x: parent.width*0.1
            y: parent.height/2-2*logbtn.height-40
            width: parent.width*0.8
            height: unitRowHeight
            Text {
                id: unmT
                width: parent.width*0.4
                height: parent.height
                text: qsTr("手机号:")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight
                font.pixelSize: height*0.8
            }
            TextInput {
                id: username
                anchors.right: parent.right
                width: parent.width*0.6
                height: parent.height
                color: "#000000"
                clip: true
                autoScroll: true
                selectionColor: "#000000"
                horizontalAlignment: Text.AlignLeft
                font.pixelSize: height*0.8
                Rectangle{
                    width: parent.width
                    height: parent.height
                    color: "#b38a8a8a"
                    radius: 8
                }
            }


        }
        Rectangle {
            id: passwdrow
            color: "#00000000"
            x: parent.width*0.1
            y: parent.height/2-logbtn.height-20
            width: parent.width*0.8
            height: unitRowHeight

            Text {
                id: pwdT
                width: parent.width*0.4
                height: parent.height
                text: qsTr("密  码:")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignRight
                font.pixelSize: height*0.8
            }
            TextInput {
                id: password
                anchors.right: parent.right
                width: parent.width*0.6
                height: parent.height
                clip: true
                transformOrigin: Item.Center
                font.capitalization: Font.MixedCase
                font.wordSpacing: 0
                autoScroll: true
                echoMode: TextInput.Password
                font.pixelSize: height*0.8
                Rectangle{
                    width: parent.width
                    height: parent.height
                    color: "#b38a8a8a"
                    radius: 8
                }
            }
        }
        Text {
            id: regBtn
            x: parent.width/2-width/2
            y: logbtn.y+logbtn.height+20
            width: logbtn.width*0.8
            height: logbtn.height
            color: "#337afb"
            text: qsTr("还未注册？点击注册")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 12
            MouseArea{
                width: parent.width
                height: parent.height
                onClicked: {
                    mainWin.source = "Register.qml"
                    rootWin.title = "注册"
                }
            }
        }
    }
}
