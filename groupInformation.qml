import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import "."

Page {
    id: groupInfor
    title: qsTr("groupInfor")
    width: Screen.width
    height: Screen.height
    Rectangle {
        width: parent.width
        height: parent.height
        Column {
            // 竖排显示组信息
            width: parent.width
            height: parent.height * 14 / 16
            Rectangle {
                //组名显示
                width: parent.width
                height: parent.height / 8
                Rectangle {
                    //groupname
                    height: parent.height
                    width: parent.width / 5
                    radius: parent.width / 15
                    color: "orange"
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    Text {
                        id: groupNameText
                        text: qsTr("组名")
                        font.pixelSize: 35
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
                Rectangle {
                    //realgroupname
                    id: realgroupnamerect
                    height: parent.height
                    width: parent.width * 4 / 5
                    anchors.right: parent.right
                    Text {
                        id: realgroupname
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pixelSize: 35
                        anchors.verticalCenter: parent.verticalCenter
                        text: qsTr("神奇四侠")
                        color: "blue"
                    }
                }
            }
            Rectangle {
                //组ID显示
                width: parent.width
                height: parent.height / 8
                Rectangle {
                    //groupID
                    height: parent.height
                    width: parent.width / 5
                    radius: parent.width / 15
                    color: "orange"
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    Text {
                        id: groupIDText
                        text: qsTr("组ID")
                        font.pixelSize: 35
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
                Rectangle {
                    //realgroupID
                    id: realgroupIDrect
                    height: parent.height
                    width: parent.width * 4 / 5
                    anchors.right: parent.right
                    Text {
                        id: realgroupID
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pixelSize: 35
                        anchors.verticalCenter: parent.verticalCenter
                        text: qsTr("+VX_AV6487")
                        color: "blue"
                    }
                }
            }
            Rectangle {
                //组目的显示
                width: parent.width
                height: parent.height / 8
                Rectangle {
                    //groupGoal
                    height: parent.height
                    width: parent.width / 5
                    radius: parent.width / 15
                    color: "orange"
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    Text {
                        id: groupGoalText
                        text: qsTr("签到目的")
                        font.pixelSize: 35
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
                Rectangle {
                    //realgroupGoal
                    id: realgroupGoalrect
                    height: parent.height
                    width: parent.width * 4 / 5
                    anchors.right: parent.right
                    Text {
                        id: realgroupGoal
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pixelSize: 35
                        anchors.verticalCenter: parent.verticalCenter
                        text: qsTr("我们的口号是：每天签到")
                        color: "blue"
                    }
                }
            }
            Rectangle {
                //签到截止日期显示
                width: parent.width
                height: parent.height / 8
                Rectangle {
                    //groupDate
                    height: parent.height
                    width: parent.width / 5
                    radius: parent.width / 15
                    color: "orange"
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    Text {
                        id: groupDateText
                        text: qsTr("截止日期")
                        font.pixelSize: 35
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
                Rectangle {
                    //realgroupDate
                    id: realgroupDaterect
                    height: parent.height
                    width: parent.width * 4 / 5
                    anchors.right: parent.right
                    Text {
                        id: realgroupDate
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pixelSize: 35
                        anchors.verticalCenter: parent.verticalCenter
                        text: qsTr("2018-10-10")
                        color: "red"
                    }
                }
            }
            //修改信息按钮
            Rectangle {
                id: groupInforChangeButton
                width: parent.width / 5
                height: parent.height / 14
                color: "red"
                radius: width / 6
                anchors.horizontalCenter: parent.horizontalCenter
                Text {
                    id: groupInforChangeButtontext
                    text: qsTr("修改组信息")
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 30
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        groupInforChangeButton.color = "green"
                        stackView.push("changeGroupInfor.qml")
                    }
                }
            }
            Connections {
                target: GroupC
                onUserOutGroupSuccessed: {
                    console.log("退出组成功")
                }
            }
            Connections {
                target: GroupC
                onUserOutGroupFailed: {
                    console.log("退出组失败")
                }
            }
            Connections{
                target: GroupC
                onDeleteGroupSuccessed:{
                    console.log("删除组成功")
                    stackView.pop()
                    stackView.pop()
                }
            }
            Connections{
                target: GroupC
                onDeletegroupFailed:{
                    console.log("删除组失败")
                }
            }

            //退出组按钮
            Rectangle {
                height: parent.height / 20
                width: parent.width / 5
                color: "orange"
                radius: 10
                //anchors.bottom: parent.bottom
                anchors.right: parent.right
                Text {
                    id: outGroupButton
                    text: qsTr("退出该组")
                    font.pixelSize: 25
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (outGroupButton.text === "退出该组") {
                            GroupC.userOutGroup(GlobalVar.currentUserId,
                                                GlobalVar.currentGroupId)
                            return
                        }
                        if(outGroupButton.text === "删除该组"){
                            GroupC.deleteGroup(GlobalVar.currentGroupId)
                            return
                        }
                    }
                }
            }
        }
        //        //退出组按钮
        //        Rectangle{
        //            height: parent.height/20
        //            width: parent.width/5
        //            color: "orange"
        //            radius: 10
        //            anchors.bottom: parent.bottom
        //            anchors.right: parent.right
        //            Text {
        //                id: outGroupButton
        //                text: qsTr("退出该组")
        //                font.pixelSize: 20
        //                //anchors.verticalCenter: parent.verticalCenter
        //                anchors.horizontalCenter: parent.horizontalCenter
        //            }
        //        }
    }
    Component.onCompleted: {
        var objGroupInfo = GlobalVar.currentGroupInfo
        realgroupname.text = objGroupInfo.title
        realgroupID.text = objGroupInfo.groupId
        realgroupGoal.text = objGroupInfo.goal
        realgroupDate.text = objGroupInfo.deadline
        if (GlobalVar.currentUserId === objGroupInfo.leaderUserId) {
            outGroupButton.text = "删除该组"
        } else {
            outGroupButton.text = "退出该组"
        }
    }
}
