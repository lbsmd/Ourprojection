import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import "."
//这一页是用来申请加组的页面

Page{

    width: Screen.width
    height: Screen.height
    id:addGroup
    title: qsTr("addGroupPage")
    Column{
        width: parent.width
        height: parent.height
        Rectangle{
            width: parent.width
            height: parent.height/8
        TextField {
                anchors.left: parent.left
                id:groupID
                height: parent.height
                width: parent.width*4/5
                placeholderText: "请输入组ID"
                color: "red"
                //限制输入范围为11到31
                focus:true
                font.pixelSize: 30
        }
        Connections {
            target: GroupC
            onGetAllGroupsCompleted:{
                var allGroupsModel=GroupC.groupModel
                console.log("共有:"+allGroupsModel.count)
                var objGroupId=groupID.text.trim()
                for(var i=0;i<allGroupsModel.count;i++){
                    var temp=allGroupsModel.get(i)
                    console.log("当前组id:"+temp.objectId)
                    if(temp.objectId===objGroupId){
                        console.log("找到目标组")
                        realuserSexual.text=temp.objectId
                        realname.text=temp.groupName
                        realusermail.text=temp.leader.username
                        realID.text=""+temp.location.latitude+","+temp.location.longitude
                        realAge.text=temp.deadline.iso
                        return
                    }
                }
            }
        }
        //searchButton
        Rectangle{
            id:searchButton
            height: parent.height
            width: parent.width/5
            radius: 30
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            color: "green"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    console.log("开始搜索")
                    GroupC.getAllGroups()
                }
            }
            Text {
                id: searchButtonText
                text: qsTr("Search")
                anchors.centerIn: parent
                font.pixelSize: 30
            }
        }
    }

    //显示搜索到的组信息

        Column{
                width: parent.width
                height: parent.height*7/8
            Rectangle {
                width: parent.width
                height: parent.height/12
            }
//GroupID
            Rectangle{
                height: parent.height/9
                width: parent.width
                color: "yellow"
                Text {
                    id: userSexual
                    text: "     GroupID"
                    font.pixelSize: 30
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                }
                Text {
                    id: realuserSexual
                    anchors.right: parent.right
                    font.pixelSize: 30
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
//Groupname
            Rectangle{
                height: parent.height/9
                width: parent.width
                color: "yellow"
                Text {
                    id: groupname
                    text:"      GroupName"
                    font.pixelSize: 30
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                }
                Text {
                    id: realname
                    anchors.right: parent.right
                    font.pixelSize: 30
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
//发起人ID
            Rectangle{
                height: parent.height/9
                width: parent.width
                color: "yellow"
                Text {
                    id: usermail
                    text: "     发起人ID"
                    font.pixelSize: 30
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                }
                Text {
                    id: realusermail
                    anchors.right: parent.right
                    font.pixelSize: 30
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
//     签到地址
            Rectangle{
                height: parent.height/9
                width: parent.width
                color: "orange"
                Text {
                    id: userID
                    text:"      签到地址"
                    font.pixelSize:30
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                }
                Text {
                    id: realID
                    anchors.right: parent.right
                    font.pixelSize: 30
                    anchors.verticalCenter: parent.verticalCenter
                }

            }
//       签到截止日期
            Rectangle{
                height: parent.height/9
                width: parent.width
                color: "orange"
                Text {
                    id: userAge
                    text: "     签到截止日期"
                    font.pixelSize: 30
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                }
                Text {
                    id: realAge
                    anchors.right: parent.right
                    font.pixelSize: 30
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
            Rectangle{
                height: parent.height/28
                width: parent.width
            }
            Connections{
                target: GroupC
                onUserJoinGroupSuccessed:{
                    console.log("加入组成功")
                }
            }
            Connections{
                target: GroupC
                onUserJoinGroupFailed:{
                    console.log("加入组失败")
                }
            }
            Rectangle{
                id: addAgreeButton
                height: parent.height/18
                width: parent.width
                radius: 30
                color: "red"
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        console.log("I want to join the Group: "+groupID.text+" !")
                        addAgreeButton.color="green"
                        GroupC.userJoinGroup(GlobalVar.currentUserId,realuserSexual.text)
                    }
                }
                Text {
                    text: qsTr("申请加入")
                    anchors.centerIn: parent
                    font.pixelSize: 35
                }
            }
        }
//申请加入按钮

    }
}
