import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import "."

//这一页是显示自己的信息
Page {
    title: qsTr("informationpage")
    id: page2item
    width: Screen.width
    height: Screen.height
    Image {
        id: page2image
        width: parent.width
        height: parent.height
        source: "qrc:/3.jpg"
    }
    Column {
        width: parent.width
        height: parent.height
        Rectangle {
            width: parent.width
            height: parent.height / 8

            Rectangle {
                // header image
                height: parent.height
                width: parent.width
                Text {
                    id: head1
                    text: "header"
                    font.pixelSize: 34
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                }
                Rectangle {
                    id: image
                    width: parent.height
                    height: parent.height
                    anchors.right: parent.right
                    radius: parent.height/2
                    anchors.verticalCenter: parent.verticalCenter
                    color: "orange"
                    Text {
                        id: realimagenametext
                        text: realname
                        font.pixelSize: parent.height*3/7
                        anchors.centerIn: parent
                    }
//                    Image {
//                        id: realimage
//                        anchors.fill: parent
//                        source: "qrc:/7.jpg"
//                    }
                }
            }
        }

        //name
        Rectangle {
            height: parent.height / 14
            width: parent.width
            color: "yellow"
            radius: 30
            Text {
                id: namenote
                text: "      Name"
                font.pixelSize: 34
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
            }
            Text {
                id: realname
                text: qsTr("WuXun")
                anchors.right: parent.right
                font.pixelSize: 34
                anchors.verticalCenter: parent.verticalCenter
            }
        }
//        //userID
//        Rectangle {
//            height: parent.height / 14
//            width: parent.width
//            color: "orange"
//            Text {
//                id: userID
//                text: "      userID"
//                font.pixelSize: 34
//                anchors.verticalCenter: parent.verticalCenter
//                anchors.left: parent.left
//            }
//            Text {
//                id: realID
//                text: "84924650"
//                anchors.right: parent.right
//                font.pixelSize: 34
//                anchors.verticalCenter: parent.verticalCenter
//            }
//        }
        Rectangle {
            height: parent.height / 14
            width: parent.width
            color: "orange"
            radius: 30
            Text {
                id: userAge
                text: "     userAge"
                font.pixelSize: 34
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
            }
            Text {
                id: realAge
                text: qsTr("15")
                anchors.right: parent.right
                font.pixelSize: 34
                anchors.verticalCenter: parent.verticalCenter
            }
        }
        Rectangle {
            height: parent.height / 14
            width: parent.width
            color: "yellow"
            radius: 30
            Text {
                id: userSexual
                text: "     userSexual"
                font.pixelSize: 34
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
            }
            Text {
                id: realuserSexual
                text: qsTr("Male")
                anchors.right: parent.right
                font.pixelSize: 34
                anchors.verticalCenter: parent.verticalCenter
            }
        }
        Rectangle {
            height: parent.height / 14
            width: parent.width
            color: "orange"
            radius: 30
            Text {
                id: usermail
                text: "     userMail"
                font.pixelSize: 34
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
            }
            Text {
                id: realusermail
                text: qsTr("84924650@qq.com")
                anchors.right: parent.right
                font.pixelSize: 34
                anchors.verticalCenter: parent.verticalCenter
            }
        }

        //手机号
        Rectangle {
            height: parent.height / 14
            width: parent.width
            color: "yellow"
            radius: 30
            Text {
                id: userPhone
                text: "      userPhone"
                font.pixelSize: 34
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
            }
            Text {
                id: realuserPhone
                text: qsTr("18843114093")
                anchors.right: parent.right
                font.pixelSize: 34
                anchors.verticalCenter: parent.verticalCenter
            }
        }
    }
    Component.onCompleted: {
        var userInfo=GroupC.getUserInfo()
        console.log("用户信息获取结果:"+userInfo)
        realname.text=userInfo.username+"   "
        realAge.text=userInfo.age+"   "
        realuserSexual.text=qsTr(userInfo.gender+"   ")
        realusermail.text=userInfo.email+"   "
        realuserPhone.text=userInfo.mobilePhoneNumber+"   "
        realimagenametext.text=realname.text.charAt(0)+realname.text.charAt(1)
    }
}
