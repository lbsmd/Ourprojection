import QtQuick 2.0

Item {
    id: toastRoot
    visible: false
    width: 60
    height: 20
    Rectangle{
        id:toastContainer
        width: parent.width
        height: parent.height
        color: "#000000"
        radius: 12
        Text {
            id: toastContent
            width: parent.width
            height: parent.height
            color: "#ffffff"
            text: qsTr("字体测试内容")
            font.pointSize: toastContainer.height*0.6
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
    SequentialAnimation{
        id:seqAni
        ParallelAnimation{
            id:parAniIn
            ColorAnimation {
                id: toastContainerIn
                target: toastContainer
                property: "color"
                from: "#00ffffff"
                to: "#000000"
                duration: 200
            }
            ColorAnimation {
                id: toastContentIn
                target: toastContent
                property: "color"
                from: "#00000000"
                to: "#ffffff"
                duration: 200
            }
        }
        ParallelAnimation{
            id:parAniOut
            ColorAnimation {
                id: toastContainerOut
                target: toastContainer
                property: "color"
                from: "#000000"
                to: "#00ffffff"
                duration: 200
            }
            ColorAnimation {
                id: toastContentOut
                target: toastContent
                property: "color"
                from: "#ffffff"
                to: "#00000000"
                duration: 200
            }
        }
    }
    function showToast(toastText,time){
        toastRoot.visible=true
        toastContent.text=toastText
        toastContainerIn.duration=time/2
        toastContainerOut.duration=time/2
        toastContentIn.duration=time/2
        toastContentOut.duration=time/2
        seqAni.start()
    }
}
