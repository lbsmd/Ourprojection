import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import "."

//这一页是用来修改自己信息的
Page {
    width: Screen.width
    height: Screen.height
    id: changeUserinfor
    title: qsTr("changeUserinforPage")
    Rectangle {
        height: parent.height
        width: parent.width
        id: changerect
        Column {
            width: parent.width
            height: parent.height * 14 / 16
            //Name
            Rectangle {
                width: parent.width
                height: parent.height / 8
                TextField {
                    id: changeName
                    anchors.right: parent.right
                    height: parent.height
                    width: parent.width * 4 / 5
                    color: "red"
                    focus: true
                    font.pixelSize: 35
                }
                //nameButton
                Rectangle {
                    id: namewordrect
                    height: parent.height
                    width: parent.width / 5
                    color: "white"
                    Text {
                        id: changenametext
                        text: qsTr("昵称")
                        anchors.centerIn: parent
                        font.pixelSize: 35
                    }
                }
            }
            //Sexual
            Rectangle {
                id: sexual
                height: parent.height / 8
                width: parent.width
                color: "orange"
                Row {
                    width: parent.width
                    height: parent.height
                    Rectangle {
                        id: sexualwordrect
                        height: parent.height
                        width: parent.width / 5
                        color: "white"
                        Text {
                            id: changesexualtext
                            text: qsTr("性别")
                            anchors.centerIn: parent
                            font.pixelSize: 35
                        }
                    }
                    RadioButton {
                        id: man
                        font.pixelSize: 35
                        text: qsTr("男")
                        checked: true
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    RadioButton {
                        id: woman
                        font.pixelSize: 35
                        text: qsTr("女")
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
            }
            //Age
            Rectangle {
                id: agerect
                width: parent.width
                height: parent.height / 8
                Rectangle {
                    id: agenoteRect
                    height: parent.height
                    width: parent.width / 5
                    color: "white"
                    Text {
                        id: changeAgetext
                        text: qsTr("年龄")
                        anchors.centerIn: parent
                        font.pixelSize: 35
                    }
                }
                Rectangle {
                    height: parent.height
                    width: parent.width * 4 / 5
                    anchors.right: parent.right
                    SpinBox {
                        id:changeAgeReal
                        width: parent.width / 3
                        height: parent.height / 2
                        anchors.centerIn: parent
                        editable: true
                    }
                }
            }

            ////手机号
            //  Rectangle{
            //  width: parent.width
            //  height: parent.height/8
            //  id:phonenumber
            //  TextField {
            //          id:changephonetextfield
            //          anchors.right: parent.right
            //          height: parent.height
            //          width: parent.width*4/5
            //          placeholderText: "15576769523"
            //          color: "red"
            //          focus:true
            //          font.pixelSize: 35
            //  }
            //  Rectangle{
            //      id:phonenumberrect
            //      height: parent.height
            //      width: parent.width/5
            //      color: "white"
            //      Text {
            //          id: changephonetext
            //          text: qsTr("Phone")
            //          anchors.centerIn: parent
            //          font.pixelSize: 35
            //      }
            //  }
            //}
            //E-mail
            Rectangle {
                width: parent.width
                height: parent.height / 8
                id: mail
                TextField {
                    id: changemailtextfield
                    anchors.right: parent.right
                    height: parent.height
                    width: parent.width * 4 / 5
                    color: "red"
                    focus: true
                    font.pixelSize: 35
                }
                Rectangle {
                    id: mailrect
                    height: parent.height
                    width: parent.width / 5
                    color: "white"
                    Text {
                        id: changemailtext
                        text: qsTr("邮箱")
                        anchors.centerIn: parent
                        font.pixelSize: 35
                    }
                }
            }
            Connections{
                target: GroupC
                onUpdateUserInfoSuccessed:{
                    console.log("更新用户信息成功")
                }
            }
            Connections{
                target: GroupC
                onUpdateUserInfoFailed:{
                    console.log("更新用户信息失败")
                }
            }
            //提交按钮
            Rectangle {
                id: changeButton
                width: parent.width / 5
                height: parent.width / 10
                radius: 50
                color: "red"
                anchors.horizontalCenter: parent.horizontalCenter
                Text {
                    id: changeButtonText
                    text: qsTr("保存")
                    font.pixelSize: 35
                    anchors.centerIn: parent
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        changeButton.color = "green"
                        var ucname = changeName.text
                        var ucsex = man.checked ? "男" : "女"
                        var ucage = changeAgeReal.value
                        var ucmail = changemailtextfield.text
                        var cUserInfo = JSON.parse(
                                    JSON.stringify(GlobalVar.currentUserInfo))
                        cUserInfo.username = ucname
                        cUserInfo.gender = ucsex
                        cUserInfo.age = ucage
                        cUserInfo.email = ucmail
                        console.log("用户信息更新后:" + JSON.stringify(cUserInfo))
                        GroupC.updateUserInfo(cUserInfo)
                    }
                }
            }
        }
    }
}
