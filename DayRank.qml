import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import "."

Page {
    title: qsTr("日排行榜")
    id: rankPage
    width: Screen.width
    height: Screen.height
    ListModel{
        id: dayModel
    }
    Component{
        id: dayDelegate

    }
    ListView{
        id: dayList
        model: dayModel
        delegate: dayDelegate
        width: parent.width
        height: parent.height
    }

    Connections{
        target: GroupC
        onGetAllSignedofGroupSuccessed:{
            console.log("收到的日签到串:"+list)
        }
    }
    Connections{
        target: GroupC
        onGetAllSignedofGroupFailed:{
            console.log("获取日签到串失败:"+list)
        }
    }
    Component.onCompleted: {
        console.log("当前组id:"+GlobalVar.currentGroupId)
        GroupC.getAllSignedofGroup(GlobalVar.currentGroupId)
    }
}
