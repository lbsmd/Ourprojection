import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Window 2.3
import QtQuick.Layouts 1.3

import "."

Window {
    id: testWindow
    height: 640
    width: 320
    visible: true

    ListModel {
        id: listmodel
        ListElement {
            type: "GroupC.createUser"
            argSize: 6
            args: "name,passwd,gender,age,mail,phone"
        }
        ListElement {
            type: "GroupC.createGroup"
            argSize: 5
            args: "gName,leaderId,gAddress,gDeadLine,gGoal"
        }
        ListElement {
            type: "GroupC.deleteGroup"
            argSize: 1
            args: "GID"
        }
        ListElement {
            type: "GroupC.userJoinGroup"
            argSize: 2
            args: "UID,GID"
        }
        ListElement {
            type: "GroupC.userOutGroup"
            argSize: 2
            args: "UID,GID"
        }
        ListElement {
            type: "GroupC.goalGet"
            argSize: 3
            args: "UID,GID,dataTime"
        }
        ListElement {
            type: "GroupC.getAllGroups"
            argSize: 0
            args: ""
        }
        ListElement {
            type: "GroupC.userLogin"
            argSize: 2
            args: "phone,passwd"
        }
        ListElement {
            type: "GroupC.getUserInfo"
            argSize: 0
            args: ""
        }
        ListElement {
            type: "GroupC.getUserGroups"
            argSize: 1
            args: "UID"
        }
    }
    function doFunction(fn, args) {
        var res = fn.apply(this, args)
        console.log("FUNCRION RETURN:"+res.msg)
    }
    Component {
        id: listDelegate
        Frame {
            //        Item {
            height: layout.implicitHeight + 20
            width: parent.width
            ColumnLayout {
                id: layout
                anchors.fill: parent
                Text {
                    text: "FunctionName:" + type
                }

                Text {
                    text: "Args:" + args
                }
                TextField {
                    id: argText
                    placeholderText: args
                }
                Button {
                    text: "Do"
                    onClicked: {
                        var t = argText.text
                        var i = t.split(",")
                        console.log("ARGS:*" + t + "*")
                        if (t.length === 0 && argSize === 0) {
                            doFunction(eval(type), i)
                        } else if (i.length != argSize) {
                            console.error(
                                        "Given:" + i.length + ",Need:" + argSize)
                            console.error("Args Size ERROR")
                            return
                        } else {
                            doFunction(eval(type), i)
                        }
                    }
                }
            }
            //        }
        }
    }

    ListView {
        anchors.fill: parent
        id: listview
        delegate: listDelegate
        model: listmodel
    }

    Connections {
        target: GroupC
        onUserLoginSuccessed: {
            console.debug("backendTestMain" + msg)
        }
        //用这种方法接收信号
    }
    Connections{
        target: GroupC
        onGetUserGroupsSuccessed:{
            console.debug("onUserGroupsGot:"+msg)
        }
    }
}
