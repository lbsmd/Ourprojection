import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import "."
import QtPositioning 5.2



//这一页是显示已经参加的Group信息
Page {
    id: window1
    visible: true
    width: Screen.width
    height: Screen.height
    title: qsTr("Grouppage")
    PositionSource{
        id:srcinpageone
        updateInterval: 1000
        active: true;
        onPositionChanged: {
            var coord=srcinpageone.position.coordinate;
            console.log("Coordinate",coord.longitude,coord.latitude);
            addtext.text=coord.longitude
            text11.text=coord.latitude;
        }
    }
    Rectangle {
        id: container
        width: parent.width
        height: parent.height
        color: " white"



        Image {
            id: img1
            source: "qrc:/back.jpg"
            height: parent.height
            width: parent.width
        }
        ListModel {
            id: recipesModel
        }
        Component {
            id: recipeDelegate
            Rectangle {
                //it is a item before
                opacity: 1
                id: recipe
                property real detailsOpacity: 0
                width: listView.width
                height: 140
                //if choose
                color: ListView.isCurrentItem ? "black" : "transparent" //选中颜色设置
                //border.color: Qt.lighter(color, 1.1)
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (listView.currentIndex != index) {
                            listView.currentIndex = index //实现item切换
                        } else {
                            console.log(listView.listdata.get(
                                            listView.currentIndex).title)
                            GlobalVar.currentGroupIndex=listView.currentIndex
                            GlobalVar.currentGroupId = recipesModel.get(listView.currentIndex).groupId
                            GlobalVar.currentGroupInfo=recipesModel.get(listView.currentIndex)
                            stackView.push("getgoal.qml")
                        }
                    }
                }

                // A simple rounded rectangle for the background
                Rectangle {

                    //background
                    id: background
                    x: 2
                    y: 2
                    width: parent.width - x * 2
                    height: parent.height - y * 2
                    color: "yellow"
                    border.color: "orange"
                    radius: 5
                }
                Row {
                    id: topLayout
                    x: 10
                    y: 10
                    height: recipeImage.height
                    width: parent.width
                    spacing: 10
                    Image {
                        id: recipeImage
                        width: 50
                        height: 50
                        source: picture
                    }
                    Column {
                        width: background.width - recipeImage.width - 20
                        height: recipeImage.height
                        spacing: 5
                        Text {
                            text: title
                            font.bold: true
                            font.pointSize: 16
                        }
                    } // Column
                } // Row
            } // Item
        } // Component

        ListView {
            //Set  ListView
            opacity: 1
            id: listView
            property ListModel listdata: recipesModel
            clip: true
            height: 900
            width: 540
            anchors.fill: parent
            model: listdata
            delegate: recipeDelegate
            onContentYChanged: {
                if (contentY < originY && (originY-contentY)>Screen.height*0.28){
                    console.log("感受到下拉了")
                    var yonghuInfo = GroupC.getUserInfo()
                    console.log("接收用户信息结果:" + yonghuInfo)
                    console.log("用户id:" + yonghuInfo.objectId)
                    GroupC.getUserGroups(yonghuInfo.objectId)
                }
            }
        }
        Row {
            //bottom Button
            visible: false
            anchors {
                left: parent.left
                bottom: parent.bottom
                margins: 20
            }
            spacing: 10
            Rectangle {
                //   ADD ONE
                opacity: 1
                width: 180
                height: 180
                radius: 90
                color: "green"
                Text {
                    id: addtext
                    text: qsTr("Addone")
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 30
                }
                Button {
                    opacity: 0
                    anchors.fill: parent
                    onClicked: {
                        var t
                        t = listView.currentIndex + 3
                        if (t > 7)
                            t = t % 7
                        var pic = "qrc:/" + t + ".jpg"

                        recipesModel.append({
                                                title//注意这个“，” 如果没有的话运行会报，Unable to assign [undefined] to QUrl source 错误
                                                : "Vegetable Soup" + "\n" + t,
                                                picture: pic
                                            })
                        listView.currentIndex = listView.currentIndex + 1
                    }
                }
            }
            Rectangle {
                //REMOVE ALL
                id: remove
                opacity: 1
                width: 180
                height: 180
                radius: 90
                color: "green"
                Text {
                    id: text11
                    text: qsTr("Remove")
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 30
                }
                Button {
                    opacity: 0
                    anchors.fill: parent
                    //onClicked: recipesModel.clear()
                    onClicked: recipesModel.remove(listView.currentIndex)
                }
            }
        }
    }
    Connections {
        target: GroupC
        onGetUserGroupsSuccessed: {
            console.log("获取用户组列表成功")
            console.log("接收json串:" + msg)
            recipesModel.clear()
            var userGroupsObj = JSON.parse(msg)
            GlobalVar.userGroupsObject=msg
            console.log("共收到:"+userGroupsObj.length)
            var pic = "2.jpg"
            for (var i = 0; i < userGroupsObj.length; i++) {
                var groupObj = userGroupsObj[i]
                recipesModel.append({
                                        title: groupObj.groupName,
                                        picture: pic,
                                        deadline: groupObj.deadline.iso,
                                        goal: groupObj.goal,
                                        leaderUserId: groupObj.leader.objectId,
                                        location: [groupObj.location.latitude, groupObj.location.longitude],
                                        groupId: groupObj.objectId,
                                        createTime: groupObj.createdAt
                                    })
            }
        }
    }
    Connections {
        target: GroupC
        onGetUserGroupsFailed: {
            console.log("获取用户组列表失败")
            console.log("接收json串:" + msg)
        }
    }

    Component.onCompleted: {
        var yonghuInfo = GroupC.getUserInfo()
        console.log("接收用户信息结果:" + yonghuInfo)
        console.log("用户id:" + yonghuInfo.objectId)
        GroupC.getUserGroups(yonghuInfo.objectId)
    }

    //    Rectangle {
    //    opacity: 0.5
    //    z:2
    //    width:540;height:800
    //    ListModel{  //数据模型
    //    id:listModel
    //    ListElement{name:"Tom";number:"”001″"}
    //    ListElement{name:"”John”";number:"”002″"}
    //    ListElement{name:"”Sum”";number:"”003″"}
    //    }

    //    Component{     //代理
    //    id:delegate
    //    Item{
    //        id:wrapper
    //        width:540; height:100
    //        Row{
    //        Rectangle{
    //            id:rect4
    //            width: 50
    //            height: 50
    //            radius: 25
    //            color: "blue"
    //        }

    //    Column{
    //        id:colume1
    ////    x:5; y:5
    //    Text{text:"”<b>Name:</b>”"+name}
    //    Text{text:"”<b>Number:</b>”"+number}
    //    }
    //    }
    //    }
    //    }
    //    Component{   //高亮条
    //    id:highlight
    //    Rectangle{color:"lightsteelblue";radius:5}
    //    }
    //    ListView{  //视图
    //    width:parent.width; height:parent.height
    //    model:listModel  //关联数据模型
    //    delegate:delegate  //关联代理
    //    highlight:highlight  //关联高亮条
    //    focus:true  //可以获得焦点，这样就可以响应键盘了
    //    }
    //    }

    //    MainForm {
    //        anchors.fill: parent
    //        mouseArea.onClicked: {
    //            console.log(qsTr('Clicked on background. Text: "' + textEdit.text + '"'))
    //        }
    //    }
}
