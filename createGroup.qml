import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import "."
import QtPositioning 5.8
Page {
    id: createGroupPage
    title: qsTr("createGroupPage")
    width: Screen.width
    height: Screen.height
    PositionSource{
        id:srcincreategrouppage
        updateInterval: 1000
        active: true;
        onPositionChanged: {
            var coord=srcincreategrouppage.position.coordinate;
            console.log("Coordinate",coord.longitude,coord.latitude);
            //addtext.text=coord.longitude
            //text11.text=coord.latitude;
        }
    }

    Rectangle {
        Image {
            id: backimangeincreategroup
            source: "qrc:/8.jpg"
            anchors.fill: parent
        }
        height: parent.height
        width: parent.width
        Column {
            width: parent.width
            height: parent.height * 14 / 16
            //填写组名
            Rectangle {
                width: parent.width
                height: parent.height / 8
                TextField {
                    id: createGroupName
                    anchors.right: parent.right
                    height: parent.height
                    width: parent.width * 4 / 5
                    placeholderText: "组名"
                    color: "red"
                    focus: true
                    font.pixelSize: 24
                }
                Rectangle {
                    id: creategroupnamewordrect
                    height: parent.height
                    width: parent.width / 5
                    color: "white"
                    Text {
                        id: changeGroupNametext
                        text: qsTr("组名")
                        anchors.centerIn: parent
                        font.pixelSize: 24
                    }
                }
            }
            //填写签到地点
            Rectangle {
                width: parent.width
                height: parent.height / 8
                TextField {
                    id: createGroupAdd
                    anchors.right: parent.right
                    height: parent.height
                    width: parent.width * 4 / 5
                    placeholderText: "签到地址"
                    color: "red"
                    focus: true
                    font.pixelSize: 24
                }
                Rectangle {
                    id: creategroupAddwordrect
                    height: parent.height
                    width: parent.width / 5
                    color: "white"
                    Text {
                        id: changeGroupAddtext
                        text: qsTr("签到地址")
                        anchors.centerIn: parent
                        font.pixelSize: 24
                    }
                }
            }
            //签到目的
            Rectangle {
                width: parent.width
                height: parent.height / 8
                TextField {
                    id: createGroupintence
                    anchors.right: parent.right
                    height: parent.height
                    width: parent.width * 4 / 5
                    placeholderText: "签到目的"
                    color: "red"
                    focus: true
                    font.pixelSize: 24
                }
                Rectangle {
                    id: creategroupintencewordrect
                    height: parent.height
                    width: parent.width / 5
                    color: "white"
                    Text {
                        id: changeGroupintencetext
                        text: qsTr("签到目的")
                        anchors.centerIn: parent
                        font.pixelSize: 24
                    }
                }
            }
            //签到截止日期
            Rectangle {
                width: parent.width
                height: parent.height / 8
                Rectangle {
                    id: creategroupDatewordrect
                    height: parent.height
                    width: parent.width / 5
                    color: "white"
                    Text {
                        id: createGroupDatetext
                        text: qsTr("结束时间")
                        anchors.centerIn: parent
                        font.pixelSize: 24
                    }
                }
                Rectangle {
                    height: parent.height
                    width: parent.width * 4 / 5
                    anchors.right: parent.right
                    Row {
                        height: parent.height
                        width: parent.width
                        SpinBox {
                            //year
                            id: year
                            width: parent.width / 3
                            height: parent.height / 2
                            editable: true
                            from: 2018
                            to: 2050
                            anchors.verticalCenter: parent.verticalCenter
                        }
                        SpinBox {
                            //month
                            id: month
                            width: parent.width / 3
                            height: parent.height / 2
                            editable: true
                            from: 1
                            to: 12
                            anchors.verticalCenter: parent.verticalCenter
                        }
                        SpinBox {
                            //day
                            id: day
                            width: parent.width / 3
                            height: parent.height / 2
                            editable: true
                            from: 1
                            to: 31
                            anchors.verticalCenter: parent.verticalCenter
                        }
                    }
                }
            }
            Connections{
                target: GroupC
                onCreateGroupSuccessed:{
                    console.log("创建组成功"+msg)
                }
            }
            Connections{
                target: GroupC
                onCreateGroupFailed:{
                    console.log("创建组失败:"+msg)
                }
            }

            //提交按钮
            Rectangle {
                id: createGroupButton
                width: parent.width / 5
                height: parent.width / 10
                radius: 50
                color: "red"
                anchors.horizontalCenter: parent.horizontalCenter
                Text {
                    id: createGroupButtonText
                    text: qsTr("发起签到")
                    font.pixelSize: 24
                    anchors.centerIn: parent
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        createGroupButton.color = "green"
                        var groupName=createGroupName.text
                        var groupAddr=createGroupAdd.text
                        var groupIntence=createGroupintence.text
                        var groupDeadLine=new Date()
                        groupDeadLine.setFullYear(year.value,month.value,day.value)
                        GroupC.createGroup(groupName,GlobalVar.currentUserId,
                                           groupAddr,groupDeadLine,groupIntence)
                    }
                }
            }
        }
    }
}
